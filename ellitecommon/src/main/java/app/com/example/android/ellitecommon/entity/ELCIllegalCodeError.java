
package app.com.example.android.ellitecommon.entity;

import com.google.gsonhtcfix.annotations.SerializedName;


public class ELCIllegalCodeError implements ILCDataModel{


	private static final long serialVersionUID = 1L;
	
	@SerializedName("error")
	private String mError;
	
	@SerializedName("status")
	private ELCStatusError mStatusError;


	public ELCStatusError getStatusError() {
		return mStatusError;
	}

	public String getError() {
		return mError;
	}
	
	public void setStatusError(ELCStatusError statusError) {
		this.mStatusError = statusError;
	}
	
	

}
