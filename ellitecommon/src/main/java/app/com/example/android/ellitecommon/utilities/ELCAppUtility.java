
package app.com.example.android.ellitecommon.utilities;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Locale;

public class ELCAppUtility {


    public static void log(String tag, String inMsg) {
        if (TextUtils.isEmpty(tag) || TextUtils.isEmpty(inMsg)) return;
        ELCAppCommonUtility.log(tag, inMsg);
    }

    public static int getScreenWidth(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        int width;
        if (activity != null && !activity.isFinishing()) {
            activity.getWindowManager().getDefaultDisplay()
                    .getMetrics(displayMetrics);
            width = displayMetrics.widthPixels;
        } else {
            width = 0;
        }
        return width;
    }

    public static int getScreenHeight(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay()
                .getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        return height;
    }

    public static int getOSVersion() {
        return Build.VERSION.SDK_INT;
    }

    public static void setLatotoFont(Context context, TextView view,
                                     String fontType) {
        if (view == null || context == null)
            return;

        AssetManager am = context.getApplicationContext().getAssets();
        Typeface typeface = Typeface.createFromAsset(am,
                String.format(Locale.US, "fonts/%s", fontType));
        view.setTypeface(typeface);
    }

    public static boolean isNetworkAvailable(Context context) {
        if (context == null) return false;
        ConnectivityManager ConnectMgr = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (ConnectMgr == null)
            return false;
        NetworkInfo NetInfo = ConnectMgr.getActiveNetworkInfo();
        if (NetInfo == null)
            return false;

        return NetInfo.isConnected();
    }

    public static int getResolutionType(Activity activity) {
        if (activity == null || activity.isFinishing()) {
            return 0;
        } else {
            DisplayMetrics dm = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
            int density = dm.densityDpi;
            if (density == DisplayMetrics.DENSITY_LOW) {
                return ELCConstants.SCREEN_DENSITY_LOW;
            } else if (density == DisplayMetrics.DENSITY_MEDIUM) {
                return ELCConstants.SCREEN_DENSITY_MEDIUM;
            } else if (density == DisplayMetrics.DENSITY_HIGH) {
                return ELCConstants.SCREEN_DENSITY_HIGH;
            } else {
                return ELCConstants.SCREEN_DENSITY_EXTRA_HIGH;
            }
        }
    }

    public static int getScreenGridUnit(Context context) {
        return getScreenWidth((Activity) context)
                / ELCConstants.NUMBER_OF_BOX_IN_ROW;
    }


    public static Bitmap blurRenderScript(Bitmap bitmap, int radius, Context context) {
        if (Build.VERSION.SDK_INT >= 17) {
            try {
                try {
                    bitmap = RGB565toARGB888(bitmap);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Bitmap newBitmap = Bitmap.createBitmap(
                        bitmap.getWidth(), bitmap.getHeight(),
                        Bitmap.Config.ARGB_8888);

                RenderScript renderScript = RenderScript.create(context);

                Allocation blurInput = Allocation.createFromBitmap(renderScript, bitmap);
                Allocation blurOutput = Allocation.createFromBitmap(renderScript, newBitmap);

                ScriptIntrinsicBlur blur = ScriptIntrinsicBlur.create(renderScript,
                        Element.U8_4(renderScript));
                blur.setInput(blurInput);
                blur.setRadius(radius); // radius must be 0 < r <= 25
                blur.forEach(blurOutput);

                blurOutput.copyTo(newBitmap);
                renderScript.destroy();

                return newBitmap;
            } catch (Exception e) {
            }
        }
        return bitmap;

    }

    public static Bitmap RGB565toARGB888(Bitmap img) throws Exception {
        int numPixels = img.getWidth() * img.getHeight();
        int[] pixels = new int[numPixels];

        //Get JPEG pixels.  Each int is the color values for one pixel.
        img.getPixels(pixels, 0, img.getWidth(), 0, 0, img.getWidth(), img.getHeight());

        //Create a Bitmap of the appropriate format.
        Bitmap result = Bitmap.createBitmap(img.getWidth(), img.getHeight(), Bitmap.Config.ARGB_8888);

        //Set RGB pixels.
        result.setPixels(pixels, 0, result.getWidth(), 0, 0, result.getWidth(), result.getHeight());
        return result;
    }


    public static void measureView(View child) {
        try {
            ViewGroup.LayoutParams p = child.getLayoutParams();
            if (p == null) {
                p = new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
            }

            int childWidthSpec = ViewGroup.getChildMeasureSpec(0, 0, p.width);
            int lpHeight = p.height;
            int childHeightSpec;
            if (lpHeight > 0) {
                childHeightSpec = View.MeasureSpec.makeMeasureSpec(lpHeight, View.MeasureSpec.EXACTLY);
            } else {
                childHeightSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
            }
            child.measure(childWidthSpec, childHeightSpec);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static final int getColor(Context context, int id) {
        final int version = Build.VERSION.SDK_INT;
        if (version >= Build.VERSION_CODES.M) {
            return ContextCompat.getColor(context, id);
        } else {
            return context.getResources().getColor(id);
        }
    }


    public static void saveGCMRegistrationID(Context context, String id) {
        ELCSecureSharedPreferences prefs = new ELCSecureSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(ELCConstants.KEY_GCM_REGISTRATION_ID, id);
        editor.commit();
    }


    public static void saveSNSEndPoint(Context context, String id) {
        ELCSecureSharedPreferences prefs = new ELCSecureSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(ELCConstants.KEY_SNS_END_POINT, id);
        editor.commit();
    }

    public static String getSNSEndPoint(Context context) {
        try {
            ELCSecureSharedPreferences prefs = new ELCSecureSharedPreferences(context);
            String id = prefs.getString(ELCConstants.KEY_SNS_END_POINT, "");
            return id;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * The method return camel case of argument
     * @param inputString String make to be camel case
     * @return Camel case of the argument string
     */
    public static String toCamelCase(String inputString) {
        String result = "";
        if (inputString.length() == 0) {
            return result;
        }
        char firstChar = inputString.charAt(0);
        char firstCharToUpperCase = Character.toUpperCase(firstChar);
        result = result + firstCharToUpperCase;
        for (int i = 1; i < inputString.length(); i++) {
            char currentChar = inputString.charAt(i);
            char previousChar = inputString.charAt(i - 1);
            if (previousChar == ' ') {
                char currentCharToUpperCase = Character.toUpperCase(currentChar);
                result = result + currentCharToUpperCase;
            } else {
                char currentCharToLowerCase = Character.toLowerCase(currentChar);
                result = result + currentCharToLowerCase;
            }
        }
        return result;
    }

}
