package app.com.example.android.ellitecommon.utilities;

public class ELCConstants {


    public static final int SCREEN_DENSITY_LOW = 1;
    public static final int SCREEN_DENSITY_MEDIUM = 2;
    public static final int SCREEN_DENSITY_HIGH = 4;
    public static final int SCREEN_DENSITY_EXTRA_HIGH = 8;

    public static final String KEY_UNIQUE_ID = "uniqueid";
    public static final String KEY_IMEI = "imei";
    public static final int NUMBER_OF_BOX_IN_ROW = 16;
    public static final int NUNBER_OF_BOX_IN_HEIGHT_ACTION_BAR = 1;



    public static final int NUMBER_OF_BOX_IN_HEIGHT = 56;

    public static final String Title = "title";

    public static final String PARSE_ERROR = "parsing_error";


    public static final int ERROR_CODE_499 = 499;
    public static final int ERROR_CODE_449 = 449;
    public static final int ERROR_CODE_502 = 502;
    public static final int ERROR_CODE_503 = 503;
    public static final int ERROR_CODE_504 = 504;
    public static final int ERROR_CODE_401 = 401;
    public static final int ERROR_CODE_410 = 410;
    public static final int ERROR_CODE_400 = 400;
    public static final String FAILURE = "failure";
    public static final String FAILURE_ERROR = "failure_error";

    public static final String all = "textToSearch";

    public static final int ERROR_CODE_403 = 403;



    public static String KEY_GCM_REGISTRATION_ID = "key_registration_id";
    public static String KEY_SNS_END_POINT = "key_sns_end_point";






}

