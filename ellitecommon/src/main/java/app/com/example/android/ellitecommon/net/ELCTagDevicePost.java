
package app.com.example.android.ellitecommon.net;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.google.gsonhtcfix.Gson;

import java.io.UnsupportedEncodingException;
import java.util.Map;

import app.com.example.android.ellitecommon.utilities.ELCAppCommonUtility;
import app.com.example.android.ellitecommon.utilities.ELCConstants;

public class ELCTagDevicePost extends Request<String> {
	
	private final Listener<String> mListener;
	private String mDataModel;
	private final Gson mGson;
    private Map<String, String> mHeaders;
    private Map<String, String> mParams;
    private final String mRequestBody;
	private String TAG = ELCTagDevicePost.class.getName();
	private String mUrl;
    
    /** Charset for request. */
    private static final String PROTOCOL_CHARSET = "utf-8";
    
    public static final int MY_SOCKET_TIMEOUT_MS = 30000;
    
    

    /** Content type for request. */
    private static final String PROTOCOL_CONTENT_TYPE =
        String.format("application/json; charset=%s", PROTOCOL_CHARSET);
	
	public ELCTagDevicePost(String url, Listener<String> listener, ErrorListener errorListener, String model, Map<String, String> params,
							Map<String, String> headers, String requestBody, int httpRequestMethod){
		super(httpRequestMethod, url, errorListener);
		mListener = listener;
		mDataModel = model;
		mGson = new Gson();
		mParams = params;
		mHeaders = headers;
		mRequestBody = requestBody;
		mUrl = url;
	}
	
	@Override
	public Request<?> setRetryPolicy(RetryPolicy retryPolicy) {
		retryPolicy = new DefaultRetryPolicy(MY_SOCKET_TIMEOUT_MS,
				DefaultRetryPolicy.DEFAULT_MAX_RETRIES , DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

		return super.setRetryPolicy(retryPolicy);
	}
	
	@Override
	protected void deliverResponse(String model) {
		mListener.onResponse(model);
	}

	
	@Override
	protected Response<String> parseNetworkResponse(NetworkResponse response) {
		String jsonString = new String(response.data);	
		ELCAppCommonUtility.log(TAG  , "json Response String: " + jsonString+" statusCode:"+response.statusCode);
		int statusCode = response.statusCode;
		try {
			ELCAppCommonUtility.log("tag", "status code:" + statusCode);
			if(jsonString == null || jsonString.trim().length() == 0) {
				throw new IllegalStateException();
			}
			if(statusCode != 200) {
				
				throw new IllegalErrorCode(jsonString);
			}	
			
			 return Response.success((String)mGson.fromJson(jsonString, mDataModel.getClass()), getCacheEntry());
		
		} catch (Exception exception) {
			exception.printStackTrace();
			VolleyError volleyError = new VolleyError(ELCConstants.PARSE_ERROR);
			volleyError.setUrl(mUrl);
			return Response.error(volleyError);
		} 
		
		
		
	}
	@Override
	protected Map<String, String> getParams() throws AuthFailureError {
		 return mParams != null ? mParams : super.getParams();
	}
	
	@Override
	public Map<String, String> getHeaders() throws AuthFailureError {
		 return mHeaders != null ? mHeaders : super.getHeaders();
	}
	@Override
	public byte[] getBody() throws AuthFailureError {
		try {
            return mRequestBody == null ? null : mRequestBody.getBytes(PROTOCOL_CHARSET);
        } catch (UnsupportedEncodingException uee) {
          
            return null;
        }
	}
	
	
	
	@Override
	public String getBodyContentType() {
		 return PROTOCOL_CONTENT_TYPE;
	}
	/**
     * @deprecated Use {@link #getBody()}.
     */
	@Override
    public String getPostBodyContentType() {
        return getBodyContentType();
    }

    /**
     * @throws AuthFailureError
     * @deprecated Use {@link #getBody()}.
     */
    @Override
    public byte[] getPostBody() throws AuthFailureError {
        return getBody();
    }
    
    private class  IllegalErrorCode extends Exception {

		private static final long serialVersionUID = 1L;

		public IllegalErrorCode(String message) {
	        super(message);
	    }
		 public String getMessage() {
		        return super.getMessage();
		  }
	}
	
}

