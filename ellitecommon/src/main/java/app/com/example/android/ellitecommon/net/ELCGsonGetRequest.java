
package app.com.example.android.ellitecommon.net;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.google.gsonhtcfix.Gson;
import com.google.gsonhtcfix.GsonBuilder;

import java.io.UnsupportedEncodingException;
import java.util.Map;

import app.com.example.android.ellitecommon.entity.ELCIllegalCodeError;
import app.com.example.android.ellitecommon.entity.ILCDataModel;
import app.com.example.android.ellitecommon.utilities.ELCAppCommonUtility;
import app.com.example.android.ellitecommon.utilities.ELCConstants;

public class ELCGsonGetRequest extends Request<ILCDataModel> {

    private final Listener<ILCDataModel> mListener;
    private ILCDataModel mDataModel;
    private Gson mGson = null;
    private Map<String, String> mHeaders;
    private String TAG = ELCGsonGetRequest.class.getName();
    public static final int MY_SOCKET_TIMEOUT_MS = 60000;
    private String mUrl;
    private String mRequestBody;
    private GsonBuilder mGsonBuider;
    /**
     * Charset for request.
     */
    private static final String PROTOCOL_CHARSET = "utf-8";


    public ELCGsonGetRequest(String url, Listener<ILCDataModel> listener, ErrorListener
            errorListener, ILCDataModel model) {
        super(Method.GET, url, errorListener);
        mListener = listener;
        mDataModel = model;
        mUrl = url;
        ELCAppCommonUtility.log(TAG  , "Request Url: " +mUrl);
    }

    public ELCGsonGetRequest(String url, Listener<ILCDataModel> listener, ErrorListener
            errorListener, ILCDataModel model, Map<String, String> headers) {
        super(Method.GET, url, errorListener);
        mListener = listener;
        mDataModel = model;
        mHeaders = headers;
        mUrl = url;

    }

    public ELCGsonGetRequest(String url, Listener<ILCDataModel> listener, ErrorListener
            errorListener, ILCDataModel model, Map<String, String> headers, String requestBody) {
        super(Method.GET, url, errorListener);
        mListener = listener;
        mDataModel = model;
        mGson = new Gson();
        mHeaders = headers;
        mUrl = url;
        mRequestBody = requestBody;
    }

    @Override
    protected void deliverResponse(ILCDataModel model) {
        mListener.onResponse(model);

    }

    @Override
    public void deliverError(VolleyError error) {
        super.deliverError(error);
    }

    @Override
    public Request<?> setRetryPolicy(RetryPolicy retryPolicy) {
        retryPolicy = new DefaultRetryPolicy(MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        return super.setRetryPolicy(retryPolicy);
    }

	/*@Override
    public void setRetryPolicy(RetryPolicy retryPolicy) {
		retryPolicy = new DefaultRetryPolicy(
				MY_SOCKET_TIMEOUT_MS,
				DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
		super.setRetryPolicy(retryPolicy);
	}*/


    @Override
    protected Response<ILCDataModel> parseNetworkResponse(NetworkResponse response) {
        String jsonString = new String(response.data);
        int statusCode = response.statusCode;
        ELCAppCommonUtility.log(TAG  , "json Response String: " + jsonString+" statusCode:"+response.statusCode);
        try {

            if (statusCode == ELCConstants.ERROR_CODE_403) {
                statusCode = ELCConstants.ERROR_CODE_401;
            }
            if (statusCode == ELCConstants.ERROR_CODE_401 || statusCode == ELCConstants.ERROR_CODE_410) {
                throw new IllegalErrorCode(jsonString);
            }
            if (statusCode == ELCConstants.ERROR_CODE_499 || statusCode == ELCConstants.ERROR_CODE_502
                    || statusCode == ELCConstants.ERROR_CODE_503 || statusCode == ELCConstants.ERROR_CODE_504) {
                throw new IllegalErrorCode(jsonString);
            }

            if (jsonString == null || jsonString.trim().length() == 0) {
                throw new IllegalStateException();
            }
            try {

                ELCIllegalCodeError errorStatus = new ELCIllegalCodeError();
                errorStatus = mGson.fromJson(jsonString, errorStatus.getClass());

                if (errorStatus != null && errorStatus.getStatusError() != null && errorStatus.getStatusError().getmResult() != null
                        && errorStatus.getStatusError().getmResult().equalsIgnoreCase(ELCConstants.FAILURE)) {
                    VolleyError volleyError = new VolleyError(ELCConstants.FAILURE_ERROR);
                    volleyError.setUrl(mUrl);
                    if (errorStatus != null && errorStatus.getStatusError() != null && errorStatus.getStatusError().getmMessage() != null) {
                        volleyError.setAlertMessage(errorStatus.getStatusError().getmMessage().getMessage());
                        volleyError.setmAlertTitle(errorStatus.getStatusError().getmMessage().getTitle());
                    }

                    return Response.error(volleyError);
                }

            } catch (Exception ex) {

            }
               /* if (mDataModel instanceof CLCNewsList) {
                   RealmList<CLCNewsItem> newsItems = null;
                    Type type = new TypeToken<RealmList<CLCNewsItem>>(){}.getType();
                    mGsonBuider = new GsonBuilder()
                            .setExclusionStrategies(new ExclusionStrategy() {
                                @Override
                                public boolean shouldSkipField(FieldAttributes f) {
                                    return f.getDeclaringClass().equals(RealmObject.class);
                                }

                                @Override
                                public boolean shouldSkipClass(Class<?> clazz) {
                                    return false;
                                }
                            });
                    mGson = mGsonBuider.create();
                    newsItems = mGson.fromJson(jsonString, type);

                    if (newsItems != null) {

                        CLCNewsList newsList = (CLCNewsList) mDataModel;
                        newsList.setResponse(newsItems);
                        newsList.parseJson(jsonString);
                        return Response.success((ILCDataModel) newsList, getCacheEntry());
                    }
                    return Response.success((ILCDataModel) mDataModel, getCacheEntry());
                }

            if (mDataModel instanceof CLCSearchNewsList) {

                CLCSearchNewsList newsList = (CLCSearchNewsList) mDataModel;
                newsList.parseSearchJson(jsonString);
               // newsList.parseJson(jsonString);
                return Response.success((ILCDataModel) newsList, getCacheEntry());
            }

            if (mDataModel instanceof CLCSearchPhotosList) {

                CLCSearchPhotosList photoList = (CLCSearchPhotosList) mDataModel;
                photoList.parseSearchJson(jsonString);
                return Response.success((ILCDataModel) photoList, getCacheEntry());
            }


            if (mDataModel instanceof CLCSearchNewsList) {

                CLCSearchNewsList newsList = (CLCSearchNewsList) mDataModel;
                newsList.parseSearchJson(jsonString);
                return Response.success((ILCDataModel) newsList, getCacheEntry());
            }

            if (mDataModel instanceof CLCSearchVideoList) {

                CLCSearchVideoList newsList = (CLCSearchVideoList) mDataModel;
                newsList.parseSearchJson(jsonString);
                return Response.success((ILCDataModel) newsList, getCacheEntry());
            }

            if (mDataModel instanceof CLCPhotosList) {
                RealmList<CLCPhoto> photoItems = null;
                Type type = new TypeToken<RealmList<CLCPhoto>>(){}.getType();
                mGsonBuider = new GsonBuilder()
                        .setExclusionStrategies(new ExclusionStrategy() {
                            @Override
                            public boolean shouldSkipField(FieldAttributes f) {
                                return f.getDeclaringClass().equals(RealmObject.class);
                            }

                            @Override
                            public boolean shouldSkipClass(Class<?> clazz) {
                                return false;
                            }
                        });
                mGson = mGsonBuider.create();
                photoItems = mGson.fromJson(jsonString, type);
                if (photoItems != null) {
                    CLCPhotosList photosList = (CLCPhotosList) mDataModel;
                    photosList.setResponse(photoItems);
                    return Response.success((ILCDataModel) photosList, getCacheEntry());
                }
                return Response.success((ILCDataModel) mDataModel, getCacheEntry());
            }

            if (mDataModel instanceof CLCVideoList) {
                RealmList<CLCVideoData> videoItems = null;
                Type type = new TypeToken<RealmList<CLCVideoData>>(){}.getType();
                mGsonBuider = new GsonBuilder()
                        .setExclusionStrategies(new ExclusionStrategy() {
                            @Override
                            public boolean shouldSkipField(FieldAttributes f) {
                                return f.getDeclaringClass().equals(RealmObject.class);
                            }

                            @Override
                            public boolean shouldSkipClass(Class<?> clazz) {
                                return false;
                            }
                        });
                mGson = mGsonBuider.create();
                videoItems = mGson.fromJson(jsonString, type);
                if (videoItems != null) {
                    CLCVideoList videoList = (CLCVideoList) mDataModel;
                    videoList.setResponse(videoItems);
                    return Response.success((ILCDataModel) videoList, getCacheEntry());
                }
                return Response.success((ILCDataModel) mDataModel, getCacheEntry());
            }


            if (mDataModel instanceof CLCPhotoSearchResults) {
                ArrayList<CLCSearchPhotos> photoItems = null;
                Type type = new TypeToken<ArrayList<CLCSearchPhotos>>(){}.getType();
                mGsonBuider = new GsonBuilder()
                        .setExclusionStrategies(new ExclusionStrategy() {
                            @Override
                            public boolean shouldSkipField(FieldAttributes f) {
                                return f.getDeclaringClass().equals(RealmObject.class);
                            }

                            @Override
                            public boolean shouldSkipClass(Class<?> clazz) {
                                return false;
                            }
                        });
                mGson = mGsonBuider.create();
                photoItems = mGson.fromJson(jsonString, type);
                if (photoItems != null) {
                    CLCPhotoSearchResults searchPhotos = (CLCPhotoSearchResults) mDataModel;
                    searchPhotos.setSearchPhotos(photoItems);
                    return Response.success((ILCDataModel) searchPhotos, getCacheEntry());
                }
                return Response.success((ILCDataModel) mDataModel, getCacheEntry());
            }

            if (mDataModel instanceof CLCVideosSearchResults) {
                ArrayList<CLCSearchVideos> photoItems = null;
                Type type = new TypeToken<ArrayList<CLCSearchVideos>>(){}.getType();
                mGsonBuider = new GsonBuilder()
                        .setExclusionStrategies(new ExclusionStrategy() {
                            @Override
                            public boolean shouldSkipField(FieldAttributes f) {
                                return f.getDeclaringClass().equals(RealmObject.class);
                            }

                            @Override
                            public boolean shouldSkipClass(Class<?> clazz) {
                                return false;
                            }
                        });
                mGson = mGsonBuider.create();
                photoItems = mGson.fromJson(jsonString, type);
                if (photoItems != null) {
                    CLCVideosSearchResults searchVideo = (CLCVideosSearchResults) mDataModel;
                    searchVideo.setSearchVideosArrayList(photoItems);
                    return Response.success((ILCDataModel) searchVideo, getCacheEntry());
                }
                return Response.success((ILCDataModel) mDataModel, getCacheEntry());
            }*/


            return Response.success((ILCDataModel) mGson.fromJson(jsonString, mDataModel.getClass()), getCacheEntry());

        } catch (IllegalErrorCode e) {
            e.printStackTrace();
            VolleyError volleyError = new VolleyError(String.valueOf(statusCode));
            volleyError.setUrl(mUrl);
            ELCIllegalCodeError errorStatus = new ELCIllegalCodeError();
            try {
                errorStatus = mGson.fromJson(jsonString, errorStatus.getClass());
                if (errorStatus != null && errorStatus.getStatusError() != null && errorStatus.getStatusError().getmMessage() != null) {
                    volleyError.setAlertMessage(errorStatus.getStatusError().getmMessage().getMessage());
                    volleyError.setmAlertTitle(errorStatus.getStatusError().getmMessage().getTitle());
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return Response.error(volleyError);
        } catch (Exception exception) {
            exception.printStackTrace();
            VolleyError volleyError = new VolleyError(ELCConstants.PARSE_ERROR);
            volleyError.setUrl(mUrl);
            return Response.error(volleyError);
        }
    }

    @Override
    public Map<String, String> getHeaders() {
        try {
            return mHeaders != null ? mHeaders : super.getHeaders();
        } catch (AuthFailureError exception) {
            exception.printStackTrace();
            return null;
        }
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        try {
            return mRequestBody == null ? null : mRequestBody.getBytes(PROTOCOL_CHARSET);
        } catch (UnsupportedEncodingException uee) {

            return null;
        }
    }


}
