
package app.com.example.android.ellitecommon.utilities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class ELCCustomDialog {
	private static boolean isDialogShowing;
	
	public static void showAlert(Context context, String title, String message){
		if(ELCCustomDialog.isDialogShowing){
			return;
		}else {
			if (((Activity) context).isFinishing() == false) {
				AlertDialog.Builder builder = new AlertDialog.Builder(context);
				builder.setTitle(title).setMessage(message).setCancelable(false);
				builder.setPositiveButton(android.R.string.ok,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								dialog.cancel();
								ELCCustomDialog.isDialogShowing = false;
							}
						});
				builder.show();
				ELCCustomDialog.isDialogShowing = true;
			}
			
		}
	}

	public static void disableDialog() {
		isDialogShowing = false;
	}
}
