
package app.com.example.android.ellitecommon.entity;

import com.google.gsonhtcfix.annotations.SerializedName;


public class ELCStatusError implements ILCDataModel {

    private static final long serialVersionUID = 1L;

    @SerializedName("result")
    private String mResult;

    @SerializedName("message")
    private ELCError mMessage;

    @SerializedName("code")
    private int mCode;

    public String getmResult() {
        return mResult;
    }

    public ELCError getmMessage() {
        return mMessage;
    }

    public int getmCode() {
        return mCode;
    }


}
