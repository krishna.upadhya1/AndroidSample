
package app.com.example.android.ellitecommon.utilities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.google.gsonhtcfix.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class ELCAppCommonUtility {

    private static String TAG = "ELCAppCommonUtility";
    public static final boolean isDebug = false;


    private static final String IMAGE_PATTERN =
            "([^\\s]+(\\.(?i)(jpg|png|gif|bmp))$)";

    public static String getDeviceID(Context context) {
        return ((TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
    }

    public static void log(String tag, String inMsg) {
        if (isDebug) {
            Log.d(tag, inMsg);
        }

    }

    public static int getScreenWidth(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        int width;
        if (activity != null && !activity.isFinishing()) {
            activity.getWindowManager().getDefaultDisplay()
                    .getMetrics(displayMetrics);
            width = displayMetrics.widthPixels;
        } else {
            width = 0;
        }
        return width;
    }

    public static int getScreenHeight(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay()
                .getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        return height;
    }

    public static String getDeviceName() {
        return Build.MANUFACTURER;
    }

    public static String getDeviceModelName() {
        return Build.MODEL;
    }


    public static int getOSVersion() {
        return Build.VERSION.SDK_INT;
    }

    public static boolean isPackageInstall(String packagename, Context context) {
        boolean isInstall = false;
        final PackageManager packageManager = context.getPackageManager();
        try {
            packageManager.getPackageInfo(packagename,
                    PackageManager.GET_ACTIVITIES);
            isInstall = true;
        } catch (PackageManager.NameNotFoundException e) {
            isInstall = false;
        }

        return isInstall;
    }

    public static String getNetworkType(Context context) {
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo info = cm.getActiveNetworkInfo();
            if (info == null || !info.isConnected())
                return null; //not connected
            if (info.getType() == ConnectivityManager.TYPE_WIFI)
                return "WIFI";
            if (info.getType() == ConnectivityManager.TYPE_MOBILE) {
                int networkType = info.getSubtype();
                switch (networkType) {
                    case TelephonyManager.NETWORK_TYPE_GPRS:
                    case TelephonyManager.NETWORK_TYPE_EDGE:
                    case TelephonyManager.NETWORK_TYPE_CDMA:
                    case TelephonyManager.NETWORK_TYPE_1xRTT:
                    case TelephonyManager.NETWORK_TYPE_IDEN: //api<8 : replace by 11
                        return "2G";
                    case TelephonyManager.NETWORK_TYPE_UMTS:
                    case TelephonyManager.NETWORK_TYPE_EVDO_0:
                    case TelephonyManager.NETWORK_TYPE_EVDO_A:
                    case TelephonyManager.NETWORK_TYPE_HSDPA:
                    case TelephonyManager.NETWORK_TYPE_HSUPA:
                    case TelephonyManager.NETWORK_TYPE_HSPA:
                    case TelephonyManager.NETWORK_TYPE_EVDO_B: //api<9 : replace by 14
                    case TelephonyManager.NETWORK_TYPE_EHRPD:  //api<11 : replace by 12
                    case TelephonyManager.NETWORK_TYPE_HSPAP:  //api<13 : replace by 15
                        return "3G";
                    case TelephonyManager.NETWORK_TYPE_LTE:    //api<11 : replace by 13
                        return "4G";
                    default:
                        return null;
                }
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * @return Number of bytes available on Internal storage
     */
    public static long getAvailableSpaceOnPhone() {
        long availableSpace = -1L;
        StatFs stat = new StatFs(Environment.getDataDirectory().getPath());
        availableSpace = (long) stat.getAvailableBlocks()
                * (long) stat.getBlockSize();

        return availableSpace;
    }

    /**
     * @return true if SD Card available on Device
     */
    public static boolean isSdCardPresent() {

        return Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED);
    }

    /**
     * @return Number of bytes available on External storage
     */
    public static long getAvailableSpaceOnSDCard() {
        long availableSpace = -1L;
        StatFs stat = new StatFs(Environment.getExternalStorageDirectory()
                .getPath());
        availableSpace = (long) stat.getAvailableBlocks()
                * (long) stat.getBlockSize();

        return availableSpace;
    }

    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = pixels;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager ConnectMgr = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (ConnectMgr == null)
            return false;
        NetworkInfo NetInfo = ConnectMgr.getActiveNetworkInfo();
        if (NetInfo == null)
            return false;

        return NetInfo.isConnected();
    }


    public static String getUniqueDeviceId(Context incontext, TelephonyManager inTelephonyManger) {

        String deviceID = null;

        try {

            deviceID = inTelephonyManger.getDeviceId();
            if (!TextUtils.isEmpty(deviceID)) {
                setSharedPref(incontext, ELCConstants.KEY_IMEI, deviceID);
                ELCAppCommonUtility.log(TAG, "IMEI number is : " + deviceID);
                return deviceID;
            }
        } catch (Exception e) {
            if (isDebug) {
                e.printStackTrace();
            }
        }
        try {
            deviceID = Settings.Secure.getString(incontext.getContentResolver(), Settings.Secure.ANDROID_ID);
            if (!TextUtils.isEmpty(deviceID)) {
                ELCAppCommonUtility.log(TAG, "Android Id is : " + deviceID);
                return deviceID;
            }
        } catch (Exception e) {
            if (isDebug) {
                e.printStackTrace();
            }
        }

        try {
            deviceID = inTelephonyManger.getSubscriberId();
            if (!TextUtils.isEmpty(deviceID)) {
                ELCAppCommonUtility.log(TAG, "Subscriber Id is : " + deviceID);
                return deviceID;
            }
        } catch (Exception e) {
            if (isDebug) {
                e.printStackTrace();
            }
        }

        try {
            deviceID = getSharedPref(incontext, ELCConstants.KEY_UNIQUE_ID);

            if (!TextUtils.isEmpty(deviceID)) {
                ELCAppCommonUtility.log(TAG, "Use existing uniqe id : " + deviceID);
                return deviceID;
            } else {
                deviceID = UUID.randomUUID().toString();
                ELCAppCommonUtility.log(TAG, "Create New uniqe id : " + deviceID);
                setSharedPref(incontext, ELCConstants.KEY_UNIQUE_ID, deviceID);
                return deviceID;
            }

        } catch (Exception e) {
            if (isDebug) {
                e.printStackTrace();
            }
        }

        return deviceID;


    }

    public static String getDeviceIdentifier(Context incontext,
                                             TelephonyManager inTelephonyManger) {

        String deviceid = getUniqueDeviceId(incontext, inTelephonyManger);
        String DeviceIdentifier = Build.MANUFACTURER + "-"
                + Build.MODEL + "-" + deviceid;

        DeviceIdentifier = DeviceIdentifier.replaceAll(" ", "");
        ELCAppCommonUtility.log(TAG, "Device identifier is : " + DeviceIdentifier);
        return DeviceIdentifier;

    }


    public static String getSharedPref(Context context, String key) {
        ELCSecureSharedPreferences prefs = new ELCSecureSharedPreferences(context);
        return prefs.getString(key, "");
    }

    public static void setSharedPref(Context context, String key, String value) {
        ELCAppCommonUtility.log(TAG, "setSharedPref : Context : " + context);
        if (context != null) {
            ELCSecureSharedPreferences prefs = new ELCSecureSharedPreferences(context);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString(key, value);
            editor.commit();
        }
    }

    public static int getLaunchCount(Context context, String key) {
        ELCSecureSharedPreferences prefs = new ELCSecureSharedPreferences(context);
        return prefs.getInt(key, 0);
    }

    public static void setLaunchCount(Context context, String key, int value) {
        ELCAppCommonUtility.log(TAG, "setSharedPref : Context : " + context);
        if (context != null) {
            ELCSecureSharedPreferences prefs = new ELCSecureSharedPreferences(context);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putInt(key, value);
            editor.commit();
        }
    }

    public static boolean getIsListUpdated(Context context, String key) {
        ELCSecureSharedPreferences prefs = new ELCSecureSharedPreferences(context);
        return prefs.getBoolean(key, false);
    }

    public static void setIsListUpdated(Context context, String key, boolean value) {
        ELCAppCommonUtility.log(TAG, "setSharedPref : Context : " + context);
        if (context != null) {
            ELCSecureSharedPreferences prefs = new ELCSecureSharedPreferences(context);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean(key, value);
            editor.commit();
        }
    }

    public static int getScreenGridUnit(Context context) {
        return getScreenWidth((Activity) context)
                / ELCConstants.NUMBER_OF_BOX_IN_ROW;
    }

    public static int getScreenGridUnitBy32(Context context) {
        return getScreenWidth((Activity) context) / 32;
    }

    public static int getScreenGridUnitInHeight(Context context) {
        return getScreenHeight((Activity) context)
                / ELCConstants.NUMBER_OF_BOX_IN_HEIGHT;
    }


    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static int getActionbarHeight(Context context) {
        int gridUnit = ELCAppCommonUtility.getScreenGridUnit(context);
        int height = gridUnit * ELCConstants.NUNBER_OF_BOX_IN_HEIGHT_ACTION_BAR;
        return height;
    }


    public static void unbindDrawables(View paramView) {
        try {
            if (paramView.getBackground() != null) {
                paramView.getBackground().setCallback(null);
                paramView.setBackgroundDrawable(null);

            }
            paramView.setOnClickListener(null);
            paramView.removeCallbacks(null);
            if ((paramView instanceof ViewGroup)) {
                for (int i = 0; i < ((ViewGroup) paramView).getChildCount(); i++) {
                   /* if (((ViewGroup)paramView).getChildAt(i) instanceof ImageView){
                        ImageView imageView = (ImageView) ((ViewGroup)paramView).getChildAt(i);
                        Drawable drawable = imageView.getDrawable();
                        if (drawable instanceof BitmapDrawable) {
                            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
                            Bitmap bitmap = bitmapDrawable.getBitmap();
                            if(bitmap != null && !bitmap.isRecycled())
                                bitmap.recycle();
                        }
                    }*/
                    unbindDrawables(((ViewGroup) paramView).getChildAt(i));
                }
                ((ViewGroup) paramView).removeAllViews();
            }
            return;
        } catch (Exception localException) {
        }
    }


    public static boolean isAppInstalled(Context context, String packageName) {
        boolean appInstalled = false;

        if (!TextUtils.isEmpty(packageName)) {
            PackageManager pm = context.getPackageManager();
            try {
                pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
                appInstalled = true;
            } catch (PackageManager.NameNotFoundException e) {
                appInstalled = false;
            } catch (Exception e) {
                appInstalled = false;
            }
            return appInstalled;
        }
        return appInstalled;
    }

    public static boolean isYoutubeAppInstalled(Context context, String packageName) {
        PackageManager pm = context.getPackageManager();
        Intent mIntent = pm.getLaunchIntentForPackage(packageName);
        if (mIntent != null) {
            return true;
        } else {
            return false;
        }
    }

   /* public static boolean isGooglePlayServicesAvailable(Context context) {
        try {
            int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
            if (resultCode != ConnectionResult.SUCCESS) {
                return false;
            }
            return true;
        } catch (Exception e) {
            if (isDebug) e.printStackTrace();
            return false;
        }
    }*/


    public static boolean hasGPSDevice(Context context) {
        try {
            final LocationManager mgr = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            if (mgr == null) {
                return false;
            }

            final List<String> providers = mgr.getAllProviders();
            if (providers == null) {
                return false;
            }

            return providers.contains(LocationManager.GPS_PROVIDER);
        } catch (Exception e) {
            if (isDebug) e.printStackTrace();
            return false;
        }
    }

   /* // This four methods are used for maintaining categories.
    public static void saveCategorisList(Context context, List<CLCNotificationCategories> categoriesList) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;

        settings = context.getSharedPreferences(ELCConstants.PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Gson gson = new Gson();
        String jsonFavorites = gson.toJson(categoriesList);

        editor.putString(ELCConstants.CATEGORIES_LOCATION, jsonFavorites);

        editor.commit();
    }
*/

 /*   public static ArrayList<CLCNotificationCategories> getCategoriesList(Context context) {
        SharedPreferences settings;
        List<CLCNotificationCategories> clcNotificationCategories;

        settings = context.getSharedPreferences(ELCConstants.PREFS_NAME,
                Context.MODE_PRIVATE);

        if (settings.contains(ELCConstants.CATEGORIES_LOCATION)) {
            String jsonFavorites = settings.getString(ELCConstants.CATEGORIES_LOCATION, null);
            Gson gson = new Gson();
            CLCNotificationCategories[] favoriteItems = gson.fromJson(jsonFavorites,
                    CLCNotificationCategories[].class);

            clcNotificationCategories = Arrays.asList(favoriteItems);
            clcNotificationCategories = new ArrayList<CLCNotificationCategories>(clcNotificationCategories);
        } else
            return null;

        return (ArrayList<CLCNotificationCategories>) clcNotificationCategories;
    }*/

    /**
     * method used to get the video ID which in youtube URL
     *
     * @param url
     * @return
     */
    public static String getYoutubeId(String url) {
        String pattern = "(?<=watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";

        Pattern compiledPattern = Pattern.compile(pattern);
        Matcher matcher = compiledPattern.matcher(url);

        if (matcher.find()) {
            return matcher.group();
        }
        return null;
    }

    /**
     * method used to get the video ID which in youtube URL
     *
     * @param ytUrl
     * @return
     */
    public static String extractYTId(String ytUrl) {
        String vId = null;
       /* Pattern pattern = Pattern.compile(
                "^https?://.*(?:youtu.be/|v/|u/\\w/|embed/|watch?v=)([^#&?]*).*$",
                Pattern.CASE_INSENSITIVE);*/
        try {
            Pattern pattern = Pattern.compile(
                    "https?:\\/\\/(?:[0-9A-Z-]+\\.)?(?:youtu\\.be\\/|youtube\\.com\\S*[^\\w\\-\\s])([\\w\\-]{11})(?=[^\\w\\-]|$)(?![?=&+%\\w]*(?:['\"][^<>]*>|<\\/a>))[?=&+%\\w]*",
                    Pattern.CASE_INSENSITIVE);

            Matcher matcher = pattern.matcher(ytUrl);
            if (matcher.matches()) {
                vId = matcher.group(1);
            }
            return vId;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return vId;
    }

    public static String getAppVersion(Context context) {
        PackageManager manager = context.getPackageManager();
        PackageInfo info = null;
        try {
            info = manager.getPackageInfo(
                    context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        if (info != null)
            return info.versionName;
        else
            return null;
    }

    public static Date dateFormatter(String dateString, String dateFormat) {
        SimpleDateFormat setDateFormatter;
        Date formattedDate = null;
        if (!TextUtils.isEmpty(dateString)) {
            setDateFormatter = new SimpleDateFormat(dateFormat, Locale.ENGLISH);
            try {
                formattedDate = setDateFormatter.parse(dateString);
//                System.out.println("Formatted Date: " + formattedDate);

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return formattedDate;
    }

    public static String getUpdatedDateString(Long seconds) {
        if (seconds < 5) {
            return "Just now";
        } else if (seconds < 60) {
            return seconds + " seconds ago";
        } else if (seconds < 120) {
            return "A minute ago";
        } else {
            long minutes = seconds / 60;
            if (minutes < 60) {
                return minutes + " minutes ago";
            } else if (minutes < 120) {
                return "An hour ago";
            } else if (minutes < (24 * 60)) {
                return (int) (Math.floor(minutes / 60)) + " hours ago";
            } else {
                int days = (int) ((minutes / 60) / 24);
                if (days < 2) {
                    return "Yesterday";
                } else if (days < 7) {
                    return days + " days ago";
                } else if (days < 14) {
                    return "Last week";
                } else if (days < 30) {
                    return (int) (Math.floor(days / 7)) + " weeks ago";
                } else if (days < 60) {
                    return "Last month";
                } else if (days < 365) {
                    return (int) (Math.floor(days / 30)) + " months ago";
                } else if (days < 731) {
                    return "Last year";
                } else {
                    return (int) (Math.floor(days / 365)) + " years ago";
                }
            }
        }
    }

    /**
     * Validate image with regular expression
     *
     * @param image image for validation
     * @return true valid image, false invalid image
     */
    public static boolean isValidImage(String image) {
        try {
            Pattern pattern = Pattern.compile(IMAGE_PATTERN);
            Matcher matcher = pattern.matcher(image);
            return matcher.matches();
        } catch (Exception e) {
            return true;
        }
    }
}
